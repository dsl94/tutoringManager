@extends('vendor.adminlte.layouts.partials.app')
@section('main-content')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Last 5 created students</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>Full name</th>
                                    <th>Email</th>
                                    <th>Phone number</th>
                                    <th>Address</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($lastFiveStudents as $student)
                                    <tr style="cursor: pointer" onclick="location.href='/students/{{$student->id}}'">
                                        <td>{{$student->name}}</td>
                                        <td>{{$student->email}}</td>
                                        <td>{{$student->phone}}</td>
                                        <td>{{$student->address}}</td>
                                        <td>
                                            <a href="/students/{{$student->id}}" class="btn btn-sm btn-info"><i
                                                        class='fa fa-info'></i></a>
                                            <a data-toggle="confirmation" data-title="Are you sure you want to delete student?"
                                               href="/students/delete/{{$student->id}}" class="btn btn-sm btn-danger"><i
                                                        class='fa fa-trash'></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Number of classes per month</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <canvas id="myChart" width="400" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Your last 5 classes</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>Course name</th>
                                    <th>Student name</th>
                                    <th>Date</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($lastFiveClasses as $class)
                                    <tr>
                                        <td>{{$class->course->title}}</td>
                                        <td>{{$class->student->name}}</td>
                                        <td>{{$class->date}}</td>
                                        <td>
                                            @if($class->student->deleted != 1 && $class->course->deleted != 1)
                                                <a href="/courses/attends/{{$class->course->id}}/{{$class->student->id}}"
                                                   class="btn btn-sm btn-success"><i
                                                            class='fa fa-book'></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Earnings per month</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <canvas id="myChart2" width="400" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('inline-scripts')
    var ctx = document.getElementById("myChart").getContext('2d');
    var labels = {!! json_encode($months) !!};
    var data = {!! json_encode($values) !!};
    var label = 'Earnings per month';
    var myChart = new Chart(ctx, {
    type: 'line',
    data: {
    labels: labels,
    datasets: [{
    label: 'Number of classes per month',
    data: data,
    borderColor: "#3e95cd",
    fill: true
    }]
    },
    options: {
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    }
    });

    var ctx2 = document.getElementById("myChart2").getContext('2d');
    var labels2 = {!! json_encode($months2) !!};
    var data2 = {!! json_encode($values2) !!};
    var label2 = 'Earnings per month';
    var myChart2 = new Chart(ctx2, {
    type: 'line',
    data: {
    labels: labels2,
    datasets: [{
    label: 'Earnings per month',
    data: data2,
    borderColor: "#3e95cd",
    fill: true
    }]
    },
    options: {
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    }
    });

    $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
    });
@stop

