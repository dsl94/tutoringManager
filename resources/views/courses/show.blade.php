@extends('vendor.adminlte.layouts.partials.app')
@section('main-content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Course information</h3>
                </div>
                <form class="form-horizontal" method="POST" action="/courses/{{$course->id}}">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">Title</label>

                            <div class="col-sm-10">
                                <input value="{{$course->title}}" type="text" class="form-control" id="title"
                                       name="title" placeholder="Title" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" id="description" name="description"
                                          placeholder="Description">{{$course->description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Edit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add student to course</h3>
                </div>
                <form class="form-horizontal" method="POST" action="/courses/{{$course->id}}/student">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="student" class="col-sm-2 control-label">Student</label>
                            <div class="col-sm-10">
                                <select name="student" id="student" class="form-control" required>
                                    @foreach($students as $student)
                                        <option value="{{ $student->id }}">{{ $student->name.' ('.$student->email.')'}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Price per class</label>

                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="price" name="price"
                                       placeholder="Price per class" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Add</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Students that attend this course</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>Full name</th>
                            <th>Email</th>
                            <th>Phone number</th>
                            <th>Address</th>
                            <th>Price per class</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($course->students as $student)
                            @if($student->deleted != 1)
                                <tr style="cursor: pointer" onclick="location.href='/students/{{$student->id}}'">
                                    <td>{{$student->name}}</td>
                                    <td>{{$student->email}}</td>
                                    <td>{{$student->phone}}</td>
                                    <td>{{$student->address}}</td>
                                    <td>{{$student->pivot->price}}</td>
                                    <td>
                                        <a href="/students/{{$student->id}}" class="btn btn-sm btn-info"><i
                                                    class="fa fa-user"></i></a>
                                        <a class="btn btn-sm btn-success"
                                           href="attends/{{$course->id}}/{{$student->id}}"><i
                                                    class="fa fa-book"></i></a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Earnings from this course</h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <canvas id="myChart" width="400" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('inline-scripts')
    var ctx = document.getElementById("myChart").getContext('2d');
    var labels = {!! json_encode($months) !!};
    var data = {!! json_encode($values) !!};
    var earning = {!! json_encode($earning) !!};
    var label = 'Earnings per month';
    var myChart = new Chart(ctx, {
    type: 'line',
    data: {
    labels: labels,
    datasets: [{
    label: 'Earnings per month',
    data: data,
    borderColor: "#3e95cd",
    fill: false
    }]
    },
    options: {
    scales: {
    yAxes: [{
    ticks: {
    beginAtZero: true
    }
    }]
    }
    }
    });
@stop
