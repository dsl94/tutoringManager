@extends('vendor.adminlte.layouts.partials.app')
@section('main-content')
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Your courses</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($courses as $course)
                                    <tr style="cursor: pointer" onclick="location.href='courses/{{$course->id}}'">
                                        <td>{{$course->title}}</td>
                                        <td>{{$course->description}}</td>
                                        <td>
                                            <a href="courses/{{$course->id}}" class="btn btn-sm btn-info"><i
                                                        class="fa fa-info"></i></a>
                                            <a data-toggle="confirmation" data-title="Are you sure you want to delete course?"
                                               href="/courses/delete/{{$course->id}}" class="btn btn-sm btn-danger"><i class='fa fa-trash'></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Create new course</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="POST" action="/courses">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title" class="col-sm-2 control-label">Title</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Title"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-sm-2 control-label">Description</label>

                                    <div class="col-sm-10">
                                <textarea class="form-control" id="description" name="description"
                                          placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Create</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Earnings by course</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="myChart" width="400" height="200"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('inline-scripts')
    var ctx = document.getElementById("myChart").getContext('2d');
    var labels = {!! json_encode($labels) !!};
    var data = {!! json_encode($values) !!};
    var label = 'Earnings by course';
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                label: 'Earnings by course',
                data: data,
                backgroundColor: [
                    "#1BC0EC",
                    "#DB4B40",
                    "#EE24B9",
                    "#408DB8",
                    "#0E75B3",
                    "#F29931",
                    "#FE8232",
                    "#41CBCA",
                    "#1AA45F",
                    "#605FA4",
                    "#29FD7B",
                    "#042540"
                ],
            }]
        }
    });
    $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
    });

@stop