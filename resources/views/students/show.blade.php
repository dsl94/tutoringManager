@extends('vendor.adminlte.layouts.partials.app')
@section('main-content')
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Student information</h3>
                        </div>
                        <form class="form-horizontal" method="POST" action="/students/{{$student->id}}">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Full name</label>

                                    <div class="col-sm-10">
                                        <input value="{{$student->name}}" type="text" class="form-control" id="name"
                                               name="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input value="{{$student->email}}" type="email" class="form-control" id="email"
                                               name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-sm-2 control-label">Phone number</label>

                                    <div class="col-sm-10">
                                        <input value="{{$student->phone}}" type="text" class="form-control" id="phone"
                                               name="phone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address" class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">
                                        <input value="{{$student->address}}" type="text" class="form-control"
                                               id="address" name="address">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="info" class="col-sm-2 control-label">Additional info</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="info"
                                                  name="info">{{$student->info}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Edit</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Student's courses</h3>
                        </div>
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <tbody>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Actions</th>
                                </tr>
                                @foreach($student->courses as $course)
                                    @if($course->deleted != 1)
                                        <tr style="cursor: pointer" onclick="location.href='/courses/{{$course->id}}'">
                                            <td>{{$course->title}}</td>
                                            <td>{{$course->description}}</td>
                                            <td>{{$course->pivot->price}}</td>
                                            <td>
                                                <a href="/courses/{{$course->id}}" class="btn btn-sm btn-info"><i
                                                            class="fa fa-info"></i></a>
                                                <a class="btn btn-sm btn-success"
                                                   href="/courses/attends/{{$course->id}}/{{$student->id}}"><i
                                                            class="fa fa-book"></i></a>
                                                </button>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Add student to course</h3>
                        </div>
                        <form class="form-horizontal" method="POST" action="/students/{{$student->id}}/course">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="course" class="col-sm-2 control-label">Course</label>
                                    <div class="col-sm-10">
                                        <select name="course" id="course" class="form-control" required>
                                            @foreach($courses as $course)
                                                <option value="{{ $course->id }}">{{ $course->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="price" class="col-sm-2 control-label">Price per class</label>

                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" id="price" name="price"
                                               placeholder="Price per class" required>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right">Add</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Comments</h3>
                        </div>
                        <form class="form-horizontal" method="POST" action="/students/{{$student->id}}/comment">
                            {!! csrf_field() !!}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="course" class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="comment" name="comment" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info pull-right" style="margin-right: 15px;">
                                        Add comment
                                    </button>
                                </div>
                                <ul class="timeline">

                                    @foreach($student->comments as $comment)
                                        <li class="time-label">
                                        <span class="bg-aqua">
                                            {{$comment->created_at->diffForHumans()}}
                                        </span>
                                        </li>
                                        <!-- /.timeline-label -->

                                        <!-- timeline item -->
                                        <li>
                                            <!-- timeline icon -->
                                            <i class="fa fa-comment bg-blue"></i>
                                            <div class="timeline-item">
                                                <div class="timeline-body">
                                                    {{$comment->comment}}
                                                </div>

                                                <div class="timeline-footer">

                                                </div>
                                            </div>
                                        </li>
                                @endforeach
                                <!-- END timeline item -->
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
