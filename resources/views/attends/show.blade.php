@extends('vendor.adminlte.layouts.partials.app')
@section('main-content')
    <div class="row">
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$student->name}}'s attendance history</h3>
                </div>
                <div class="box-body table-responsive">
                    <ul class="timeline">
                    @foreach($attends as $attend)
                        <!-- timeline time label -->
                            <li class="time-label">
                            <span class="bg-aqua">
                                {{ date('d-m-Y', strtotime($attend->date)) }}
                            </span>
                            </li>
                            <!-- /.timeline-label -->

                            <!-- timeline item -->
                            <li>
                                <!-- timeline icon -->
                                @if($attend->paid == 1)
                                    <i class="fa fa-money bg-green"></i>
                                @else
                                    <i class="fa fa-money bg-red"></i>
                                @endif
                                <div class="timeline-item">
                                    <h3 class="timeline-header">Class attended on: {{ date('d-m-Y', strtotime($attend->date)) }}</h3>
                                    <div class="timeline-body">
                                        {{$attend->comment}}
                                    </div>
                                    @if($attend->paid == 0)
                                        <div class="timeline-footer">
                                            <a href="/attends/paid/{{$attend->id}}" class="btn btn-success btn-xs">Mark
                                                as paid</a>
                                        </div>
                                    @else
                                        <div class="timeline-footer">
                                            <a data-toggle="confirmation" data-title="Are you sure you want to mark class as not paid?"
                                               href="/attends/unpaid/{{$attend->id}}" class="btn btn-warning btn-xs">Mark
                                                as unpaid</a>
                                        </div>
                                    @endif
                                </div>
                            </li>
                            <!-- END timeline item -->
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Add student's attendance</h3>
                </div>
                <form class="form-horizontal" method="POST" action="/courses/attends/{{$course->id}}/{{$student->id}}">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="datepicker" class="col-sm-2 control-label">Date of the class</label>
                            <div class="col-sm-10">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input name="date" type="text" class="form-control pull-right" id="datepicker"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <div class="checkbox icheck">
                                    <label class="">
                                        <input type="checkbox" class="iCheck" name="paid">
                                        &nbsp; User paid for class
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="comment" class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" id="comment" name="comment"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">Add</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </div>
@endsection
@section('inline-scripts')
    $(document).ready(function(){
    //Date picker
    $('#datepicker').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    autoclose: true,
    endDate: new Date()
    })

    $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue'
    });
    });

    $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
    });
@stop