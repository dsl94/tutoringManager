<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function students()
    {
        return $this->belongsToMany('App\Student', 'listens')->withPivot('price');
    }

    public function attends()
    {
        return $this->hasMany('App\Attend');
    }
}
