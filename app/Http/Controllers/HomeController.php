<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Attend;
use App\Http\Requests;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $lastFiveStudents = Auth::user()->students->where('deleted', 0)->take(5);
        $lastFiveClasses = Auth::user()->attends->take(5);

        // Get number of classes per month
        $allAttends = Auth::user()->attends;
        $numberOfClassesChart = [];
        foreach ($allAttends as $attend) {
            if ($attend->paid == 1) {
                $month = explode("-", $attend->date)[1];
                $year = explode("-", $attend->date)[0];
                $key = $month . '/' . $year;
                if (!array_key_exists($key, $numberOfClassesChart)) {
                    $numberOfClassesChart[$key] = 1;
                } else {
                    $numberOfClassesChart[$key] += 1;
                }
            }
        }
        $numberOfClassesChart = array_reverse($numberOfClassesChart);
        $months = array_keys($numberOfClassesChart);
        $values = array_values($numberOfClassesChart);

        // Get earnings per month
        $earningsChart = [];
        foreach ($allAttends as $attend) {
            if ($attend->paid == 1) {
                $month = explode("-", $attend->date)[1];
                $year = explode("-", $attend->date)[0];
                $key = $year . '/' . $month;
                $tmp = $attend->course->students->filter(function ($item) use ($attend) {
                    return $item->id == $attend->student_id;
                })->first()->pivot->price;
                if (!array_key_exists($key, $earningsChart)) {
                    $earningsChart[$key] = $tmp;
                } else {
                    $earningsChart[$key] += $tmp;
                }
            }
        }
        $earningsChart = array_reverse($earningsChart);
        $months2 = array_keys($earningsChart);
        $values2 = array_values($earningsChart);

        return view('home', compact('lastFiveStudents', 'lastFiveClasses', 'months', 'values', 'months2', 'values2'));
    }
}