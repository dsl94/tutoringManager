<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Course;
use \App\User;
use \App\Attend;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $currentUser = Auth::user();
        $courses = $currentUser->courses->where('deleted', 0);
        $allCourses = $currentUser->courses;
        $chartData = [];
        foreach ($allCourses as $course) {
            if (!array_key_exists($course->title, $chartData)) {
                $chartData[$course->title] = 0;
            }
            foreach ($course->attends as $attend) {
                if ($attend->paid == 1) {
                    $tmp = $course->students->filter(function ($item) use ($attend) {
                        return $item->id == $attend->student_id;
                    })->first()->pivot->price;
                    $chartData[$course->title] += $tmp;
                }
            }
            if ($chartData[$course->title] == 0) {
                unset($chartData[$course->title]);
            }
        }
        $labels = array_keys($chartData);
        $values = array_values($chartData);

        return view('courses.index', compact('courses', 'labels', 'values'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $course = new Course;
        $course->title = $request['title'];
        $course->description = $request['description'];
        $course->user_id = Auth::id();

        $course->save();

        return redirect('/courses');
    }

    public function show(Course $course)
    {
        $currentUser = Auth::user();
        $students = $currentUser->students->where('deleted', 0);
        $studentsThatAttend = $course->students;
        $students = $students->diff($studentsThatAttend);
        $attends = $course->attends;
        $earning = 0.0;
        $chartData = [];
        foreach ($attends as $attend) {
            if ($attend->paid == 1) {
                $month = explode("-", $attend->date)[1];
                $year = explode("-", $attend->date)[0];
                $key = $year . '/' . $month;
                $tmp = $course->students->filter(function ($item) use ($attend) {
                    return $item->id == $attend->student_id;
                })->first()->pivot->price;
                if (!array_key_exists($key, $chartData)) {
                    $chartData[$key] = $tmp;
                } else {
                    $chartData[$key] += $tmp;
                }
                $earning += $tmp;
            }
        }
        ksort($chartData);
//        $chartData = $chartData;
        $months = array_keys($chartData);
        $values = array_values($chartData);
        return view('courses.show', compact('course', 'students', 'earning', 'months', 'values', 'count'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $course = Course::find($id);
        $course->title = $request['title'];
        $course->description = $request['description'];

        $course->save();

        return redirect('courses/' . $id);
    }

    public function add(Request $request, $id)
    {
        $course = Course::find($id);
        $course->students()->attach($request['student'], array('price' => $request['price']));

        $course->save();

        return redirect('courses/' . $id);
    }

    public function delete(Course $course)
    {
        $course->deleted = 1;
        $course->save();

        return back();
    }
}
