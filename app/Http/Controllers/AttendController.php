<?php

namespace App\Http\Controllers;

use App\Attend;
use App\Course;
use App\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Course $course, Student $student)
    {
        $matchThese = ['student_id' => $student->id, 'course_id' => $course->id];
        $attends = Attend::where($matchThese)->orderby('date', 'DESC')->get();
        return view('attends.show', compact('course', 'student', 'attends'));
    }

    public function store(Request $request, Course $course, Student $student)
    {
        $request->validate([
            'date'=>'required'
        ]);

        $attend = new Attend;
        $attend->student_id = $student->id;
        $attend->course_id = $course->id;
        $attend->user_id = Auth::id();
        $attend->date = $request['date'];
        if ($request['paid'] == null)
        {
            $attend->paid = 0;
        } else
        {
            $attend->paid = 1;
        }
        $attend->comment = $request['comment'];

        $attend->save();

        return back();
    }

    public function paid(Attend $attend)
    {
        $attend->paid = 1;
        $attend->save();

        return back();
    }

    public function unpaid(Attend $attend)
    {
        $attend->paid = 0;
        $attend->save();

        return back();
    }
}
