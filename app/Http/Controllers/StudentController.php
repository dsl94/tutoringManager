<?php

namespace App\Http\Controllers;

use App\Course;
use App\StudentComment;
use Egulias\EmailValidator\Exception\CommaInDomain;
use Illuminate\Http\Request;
use \App\User;
use \App\Student;
use Illuminate\Support\Facades\Auth;
use PhpParser\Comment;

class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $currentUser = Auth::user();
        $students = $currentUser->students->where('deleted', 0);
        return view('students.index', compact('students'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required'
        ]);

        $student = new Student;
        $student->name = $request['name'];
        $student->email = $request['email'];
        $student->phone = $request['phone'];
        $student->address = $request['address'];
        $student->info = $request['info'];
        $student->user_id = Auth::id();

        $student->save();

        return redirect('/students');
    }

    public function show(Student $student)
    {
        $currentUser = Auth::user();
        $courses = $currentUser->courses->where('deleted', 0);
        $coursesThatStudentAttends = $student->courses;
        $courses = $courses->diff($coursesThatStudentAttends);
        return view('students.show', compact('student', 'courses'));
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'name'=>'required'
        ]);

        $student = Student::find($id);
        $student->name = $request['name'];
        $student->email = $request['email'];
        $student->phone = $request['phone'];
        $student->address = $request['address'];
        $student->info = $request['info'];

        $student->save();

        return back();
    }

    public function comment(Request $request, $id)
    {
        $request->validate([
            'comment'=>'required'
        ]);

        $comment = new StudentComment;
        $comment->comment = $request['comment'];
        $comment->student_id = $id;

        $comment->save();

        return back();
    }

    public function add(Request $request, $id)
    {
        $student = Student::find($id);
        $student->courses()->attach($request['course'], array('price' => $request['price']));

        $student->save();

        return back();
    }

    public function delete(Student $student)
    {
        $student->deleted = 1;
        $student->save();

        return back();
    }
}
