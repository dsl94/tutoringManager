<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Course', 'listens')->withPivot('price');
    }

    public function attends()
    {
        return $this->hasMany('App\Attend');
    }

    public function comments()
    {
        return $this->hasMany('App\StudentComment')->orderBy('created_at', 'DESC');
    }
}
