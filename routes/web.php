<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // For now send directly to login
    //return view('welcome');
    return redirect('/login');
});

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});

Route::get('/courses', 'CourseController@index');
Route::post('/courses', 'CourseController@store');
Route::get('/courses/{course}', 'CourseController@show');
Route::post('/courses/{id}', 'CourseController@edit');
Route::post('/courses/{id}/student', 'CourseController@add');
Route::get('/courses/delete/{course}', 'CourseController@delete');

Route::get('/students', 'StudentController@index');
Route::post('/students', 'StudentController@store');
Route::get('/students/{student}', 'StudentController@show');
Route::post('/students/{id}', 'StudentController@edit');
Route::post('/students/{id}/comment', 'StudentController@comment');
Route::post('/students/{id}/course', 'StudentController@add');
Route::get('/students/delete/{student}', 'StudentController@delete');

Route::get('/courses/attends/{course}/{student}', 'AttendController@show');
Route::post('/courses/attends/{course}/{student}', 'AttendController@store');
Route::get('/attends/paid/{attend}', 'AttendController@paid');
Route::get('/attends/unpaid/{attend}', 'AttendController@unpaid');